 
/*__kernel void reduce(__global float *g_odata, __global const float *g_idata, __local float *sdata,
    const unsigned int n)
{
  unsigned int tid = get_local_id(0);  
  unsigned int i = get_global_id(0);
  sdata[tid] = g_idata[i];

  barrier(CLK_LOCAL_MEM_FENCE || CLK_GLOBAL_MEM_FENCE);

  for (unsigned int s=1; s < get_local_size(0); s *= 2){
  	int index = 2 * s * tid;

  	if (index < get_local_size(0)) {
		sdata[index] += sdata[index + s];
	}
    barrier(CLK_LOCAL_MEM_FENCE || CLK_GLOBAL_MEM_FENCE);	
  }
  barrier(CLK_LOCAL_MEM_FENCE || CLK_GLOBAL_MEM_FENCE);	
  if (tid == 0) g_odata[get_group_id(0)] = sdata[0];
}*/

__kernel void calcRectangles(__global float *rectanglesAreas, const int n){

    int i = get_global_id(0);
    float x;

    if (i>=0){
        x = (i+0.5)/n;
        rectanglesAreas[i] = 4.0/(1.0 + x*x);       
      
    }

}

__kernel
void reduce(__global float* result ,__global float* buffer,__local float* scratch, __const int length) {

   int global_index = get_global_id(0);
  float accumulator = INFINITY;
  while (global_index < length) {
    float element = buffer[global_index];
    accumulator = (accumulator < element) ? accumulator : element;
    global_index += get_global_size(0);
  } 	

  // Perform parallel reduction
  int local_index = get_local_id(0);
  scratch[local_index] = accumulator;
  barrier(CLK_LOCAL_MEM_FENCE);
  for(int offset = get_local_size(0) / 2;
      offset > 0;
      offset = offset / 2) {
    if (local_index < offset) {
      float other = scratch[local_index + offset];
      float mine = scratch[local_index];
      scratch[local_index] = mine + other;
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  if (local_index == 0) {
    result[get_group_id(0)] = scratch[0];
  
  }
}

		


