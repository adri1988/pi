/*#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <string.h>
#include <CL/cl.h>*/

#define MAX_SOURCE_SIZE (0x100000)
#define min(a,b) a <= b ? a : b

// Initialization function
void init (int nWorkItems, double *input, int nWorkGroups, double *output) {

  int i;

  for (i=0; i<nWorkItems; i++)
     input[i] = i+1;
       
  for (i=0; i<nWorkGroups; i++)
     output[i] = 0.0;
}





#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
//#include <OpenCL/opencl.h>
#include <unistd.h>
#include <CL/cl.h>
#include <sys/resource.h>
#include <sys/time.h>

#define XINICIAL 0
#define XFINAL 1

//extern void inicializaGPU(cl_context *context ,cl_command_queue * command_queue, cl_program *program,cl_kernel *kernel, int gpucpu,cl_device_id *device_id,char * programCL);
extern void inicializaGPU(cl_context *context ,cl_command_queue * command_queue, cl_program *program,cl_device_id *device_id, int gpucpu ,char * programCL,int numKernels,cl_kernel *kernel,char * kernelNames, ...);
extern void errNDRANge(int err);
double get_time(){
	static struct timeval 	tv0;
	double time_, time;

	gettimeofday(&tv0,(struct timezone*)0);
	time_=(double)((tv0.tv_usec + (tv0.tv_sec)*1000000));
	time = time_/1000000;
	return(time);
}

float calcularFuncion(float x)
{
  return  (4 / (1 + (x*x)));
}

void sumaRiemann(int n)  //3.141592653588 valor de la integral definida en [0,1] la integral es 4(Arctan(1) -Arctan(0) )
{	int i;
	double x, area, pi;
	area= 0.0;
	for(i=1; i<n; i++) {
		x = (i+0.5)/n;
		area += 4.0/(1.0 + x*x);
		//makeprintf("Rectangles[%d] = %f \n",i,4.0/(1.0 + x*x));
	}
	pi = area/n;	
	printf("PI=%3.10f (s)\n", pi);
	
}



void sumaRiemannGPU(int n,int gpucpu)
{
	cl_device_id device_id;     // compute device id
	cl_context context;       // compute context	
	cl_command_queue command_queue;
	cl_program program;       // compute program
	cl_kernel kernel[2];
	int err;               // error code returned from OpenCL calls
	size_t global[1];               // global domain size
	size_t local[1];               // local domain size

	cl_mem d_rectangles;
	cl_mem d_out;
   
	
	inicializaGPU(&context ,&command_queue,&program,&device_id,gpucpu,"SumaRienReduction.cl",2,kernel,"calcRectangles","reduce");

	d_rectangles = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float)*n, NULL, NULL);
	d_out = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float)*(n), NULL, NULL);

	if (!d_rectangles || !d_out){
		printf("Error: Failed to allocate device memory!\n");
		exit(1);
	}	
	clFinish(command_queue);
	int computeUnit=4;//el equipo donde estamos haciendo este código tiene 4 compute units
	global[0] = n;
	local[0]=n/computeUnit;
	//printf("Error: contador = %d!\n",contador);
	if (clSetKernelArg(kernel[0], 0, sizeof(cl_mem), &d_rectangles) != CL_SUCCESS)
		printf("ha fallado el setKernelArg 0\n");
	if (clSetKernelArg(kernel[0], 1, sizeof(cl_int), &n) != CL_SUCCESS)
		printf("ha fallado el setKernelArg 1\n");

	if (clSetKernelArg(kernel[1], 0, sizeof(cl_mem), &d_out) != CL_SUCCESS)
		printf("ha fallado el setKernelArg 0\n");
	if (clSetKernelArg(kernel[1], 1, sizeof(cl_mem), &d_rectangles) != CL_SUCCESS)
		printf("ha fallado el setKernelArg 1\n");
	if (clSetKernelArg(kernel[1], 2, sizeof(cl_float)*local[0],NULL) != CL_SUCCESS)
		printf("ha fallado el setKernelArg 0\n");
	if (clSetKernelArg(kernel[1], 3, sizeof(cl_int), &n) != CL_SUCCESS)
		printf("ha fallado el setKernelArg 1\n");
	
  
	
	err = clEnqueueNDRangeKernel(command_queue, kernel[0], 1, NULL, global, local,0, NULL, NULL);
	errNDRANge(err);
	clFinish(command_queue);


	err = clEnqueueNDRangeKernel(command_queue, kernel[1], 1, NULL, global, local,0, NULL, NULL);
	errNDRANge(err);
	clFinish(command_queue);
		
	float *out = (float *)malloc (sizeof(float)*computeUnit);
	err = clEnqueueReadBuffer( command_queue, d_out, CL_TRUE, 0, sizeof(float)*computeUnit, out , 0, NULL, NULL );	
	errNDRANge(err);
	clFinish(command_queue);

	float pi =0;
	for (int i=0;i<computeUnit ; i++){
		pi += out[i];
	}
	printf("\n pi=%f \n",pi/n);
	
	clReleaseProgram(program);	
	clReleaseKernel(kernel[0]);	
	clReleaseKernel(kernel[1]);	
	clReleaseCommandQueue(command_queue);	
	clReleaseContext(context);

	/*for (int i=0;i<computeUnit ; i++){
		printf("out[%d]=%f \n",i,out[i]);
	}*/

	free(out);

	

}

 



int main(int argc, char **argv) {

	double t0,t1;

	//Tener menos de 3 argumentos es incorrecto
	if (argc < 3) {
		fprintf(stderr, "Uso incorrecto de los parametros. exe  PRECISION [cg]\n");
		exit(1);
	}

    int pasos = atoi(argv[1]);
   
    //printf("Empezando pi \n");

	
	switch (argv[2][0]) {
		case 'c':
			t0 = get_time();
			sumaRiemann(pasos);
			t1 = get_time();
			printf(" CPU Exection time %f ms.\n", t1-t0);
			break;
		case 'g':
			
			printf("------------------------------------------------------------\n");

			t0 = get_time();
			//printf("Ejecución en CPU\n");			
			sumaRiemannGPU(pasos,0);
			t1 = get_time();
			printf(" GPU Exection time %f ms.\n", t1-t0);

			printf("------------------------------------------------------------\n");
			t0 = get_time();
			//printf("Ejecución en GPU\n");			
			sumaRiemannGPU(pasos,1);
			t1 = get_time();
			printf(" GPU Exection time %f ms.\n\n", t1-t0);
			break;
		default:
			printf("Not Implemented yet!!\n");

	}	
}


	


