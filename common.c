#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include <sys/time.h>
#include <sys/resource.h>
#include <stdarg.h>

static struct timeval tv0;

double getMicroSeconds()
{
	double t;
	gettimeofday(&tv0, (struct timezone*)0);
	t = ((tv0.tv_usec) + (tv0.tv_sec)*1000000);

	return (t);
}

void errNDRANge(int err){
	if (err==CL_INVALID_PROGRAM_EXECUTABLE){
		printf("CL_INVALID_PROGRAM_EXECUTABLE if there is no successfully built program executable available for device associated with command_queue.. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_COMMAND_QUEUE){
		printf("CL_INVALID_COMMAND_QUEUE if command_queue is not a valid command-queue. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_KERNEL){
		printf("CL_INVALID_KERNEL if kernel is not a valid kernel object. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_CONTEXT){
		printf("CL_INVALID_CONTEXT if context associated with command_queue and kernel is not the same or if the context associated with command_queue and events in event_wait_list are not the same. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_KERNEL_ARGS){
		printf("CL_INVALID_KERNEL_ARGS if the kernel argument values have not been specified. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_WORK_DIMENSION){
		printf("CL_INVALID_WORK_DIMENSION if work_dim is not a valid value (i.e. a value between 1 and 3). Error Code=%d\n", err);
		exit(1);
	}
	if (err==CL_INVALID_WORK_GROUP_SIZE){
		printf("CL_INVALID_WORK_GROUP_SIZE if local_work_size is specified and number of work-items specified by global_work_size is not evenly divisable by size of work-group given by local_work_size or does not match the work-group size specified for kernel using the __attribute__((reqd_work_group_size(X, Y, Z))) qualifier in program source. Error Code=%d\n", err);
		exit(1);
	}
	if (err==CL_INVALID_WORK_GROUP_SIZE){
		printf("CL_INVALID_WORK_GROUP_SIZE if local_work_size is NULL and the __attribute__((reqd_work_group_size(X, Y, Z))) qualifier is used to declare the work-group size for kernel in the program source. Error Code=%d\n", err);
		exit(1);
	}
	if (err==CL_INVALID_WORK_ITEM_SIZE){
		printf("CL_INVALID_WORK_ITEM_SIZE if the number of work-items specified in any of local_work_size[0], ... local_work_size[work_dim - 1] is greater than the corresponding values specified by CL_DEVICE_MAX_WORK_ITEM_SIZES[0], .... CL_DEVICE_MAX_WORK_ITEM_SIZES[work_dim - 1]. Error Code=%d\n", err);
		exit(1);
	}
	if (err==CL_INVALID_GLOBAL_OFFSET){
		printf("CL_INVALID_GLOBAL_OFFSET if global_work_offset is not NULL. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_OUT_OF_RESOURCES){
		printf("CL_OUT_OF_RESOURCES  Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_MEM_OBJECT_ALLOCATION_FAILURE){
		printf("CL_MEM_OBJECT_ALLOCATION_FAILURE if there is a failure to allocate memory for data store associated with image or buffer objects specified as arguments to kernel. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_EVENT_WAIT_LIST){
		printf("CL_INVALID_EVENT_WAIT_LIST if event_wait_list is NULL and num_events_in_wait_list > 0, or event_wait_list is not NULL and num_events_in_wait_list is 0, or if event objects in event_wait_list are not valid events. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_OUT_OF_HOST_MEMORY){
		printf("CL_INVALID_GLOBAL_OFFSET if there is a failure to allocate resources required by the OpenCL implementation on the host. Error Code=%d\n", err);
		exit(1);
	}
}

char *err_code (cl_int err_in)
{
    switch (err_in) {

        case CL_SUCCESS :
            return (char*)" CL_SUCCESS ";
        case CL_DEVICE_NOT_FOUND :
            return (char*)" CL_DEVICE_NOT_FOUND ";
        case CL_DEVICE_NOT_AVAILABLE :
            return (char*)" CL_DEVICE_NOT_AVAILABLE ";
        case CL_COMPILER_NOT_AVAILABLE :
            return (char*)" CL_COMPILER_NOT_AVAILABLE ";
        case CL_MEM_OBJECT_ALLOCATION_FAILURE :
            return (char*)" CL_MEM_OBJECT_ALLOCATION_FAILURE ";
        case CL_OUT_OF_RESOURCES :
            return (char*)" CL_OUT_OF_RESOURCES ";
        case CL_OUT_OF_HOST_MEMORY :
            return (char*)" CL_OUT_OF_HOST_MEMORY ";
        case CL_PROFILING_INFO_NOT_AVAILABLE :
            return (char*)" CL_PROFILING_INFO_NOT_AVAILABLE ";
        case CL_MEM_COPY_OVERLAP :
            return (char*)" CL_MEM_COPY_OVERLAP ";
        case CL_IMAGE_FORMAT_MISMATCH :
            return (char*)" CL_IMAGE_FORMAT_MISMATCH ";
        case CL_IMAGE_FORMAT_NOT_SUPPORTED :
            return (char*)" CL_IMAGE_FORMAT_NOT_SUPPORTED ";
        case CL_BUILD_PROGRAM_FAILURE :
            return (char*)" CL_BUILD_PROGRAM_FAILURE ";
        case CL_MAP_FAILURE :
            return (char*)" CL_MAP_FAILURE ";
        case CL_MISALIGNED_SUB_BUFFER_OFFSET :
            return (char*)" CL_MISALIGNED_SUB_BUFFER_OFFSET ";
        case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST :
            return (char*)" CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST ";
        case CL_INVALID_VALUE :
            return (char*)" CL_INVALID_VALUE ";
        case CL_INVALID_DEVICE_TYPE :
            return (char*)" CL_INVALID_DEVICE_TYPE ";
        case CL_INVALID_PLATFORM :
            return (char*)" CL_INVALID_PLATFORM ";
        case CL_INVALID_DEVICE :
            return (char*)" CL_INVALID_DEVICE ";
        case CL_INVALID_CONTEXT :
            return (char*)" CL_INVALID_CONTEXT ";
        case CL_INVALID_QUEUE_PROPERTIES :
            return (char*)" CL_INVALID_QUEUE_PROPERTIES ";
        case CL_INVALID_COMMAND_QUEUE :
            return (char*)" CL_INVALID_COMMAND_QUEUE ";
        case CL_INVALID_HOST_PTR :
            return (char*)" CL_INVALID_HOST_PTR ";
        case CL_INVALID_MEM_OBJECT :
            return (char*)" CL_INVALID_MEM_OBJECT ";
        case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR :
            return (char*)" CL_INVALID_IMAGE_FORMAT_DESCRIPTOR ";
        case CL_INVALID_IMAGE_SIZE :
            return (char*)" CL_INVALID_IMAGE_SIZE ";
        case CL_INVALID_SAMPLER :
            return (char*)" CL_INVALID_SAMPLER ";
        case CL_INVALID_BINARY :
            return (char*)" CL_INVALID_BINARY ";
        case CL_INVALID_BUILD_OPTIONS :
            return (char*)" CL_INVALID_BUILD_OPTIONS ";
        case CL_INVALID_PROGRAM :
            return (char*)" CL_INVALID_PROGRAM ";
        case CL_INVALID_PROGRAM_EXECUTABLE :
            return (char*)" CL_INVALID_PROGRAM_EXECUTABLE ";
        case CL_INVALID_KERNEL_NAME :
            return (char*)" CL_INVALID_KERNEL_NAME ";
        case CL_INVALID_KERNEL_DEFINITION :
            return (char*)" CL_INVALID_KERNEL_DEFINITION ";
        case CL_INVALID_KERNEL :
            return (char*)" CL_INVALID_KERNEL ";
        case CL_INVALID_ARG_INDEX :
            return (char*)" CL_INVALID_ARG_INDEX ";
        case CL_INVALID_ARG_VALUE :
            return (char*)" CL_INVALID_ARG_VALUE ";
        case CL_INVALID_ARG_SIZE :
            return (char*)" CL_INVALID_ARG_SIZE ";
        case CL_INVALID_KERNEL_ARGS :
            return (char*)" CL_INVALID_KERNEL_ARGS ";
        case CL_INVALID_WORK_DIMENSION :
            return (char*)" CL_INVALID_WORK_DIMENSION ";
        case CL_INVALID_WORK_GROUP_SIZE :
            return (char*)" CL_INVALID_WORK_GROUP_SIZE ";
        case CL_INVALID_WORK_ITEM_SIZE :
            return (char*)" CL_INVALID_WORK_ITEM_SIZE ";
        case CL_INVALID_GLOBAL_OFFSET :
            return (char*)" CL_INVALID_GLOBAL_OFFSET ";
        case CL_INVALID_EVENT_WAIT_LIST :
            return (char*)" CL_INVALID_EVENT_WAIT_LIST ";
        case CL_INVALID_EVENT :
            return (char*)" CL_INVALID_EVENT ";
        case CL_INVALID_OPERATION :
            return (char*)" CL_INVALID_OPERATION ";
        case CL_INVALID_GL_OBJECT :
            return (char*)" CL_INVALID_GL_OBJECT ";
        case CL_INVALID_BUFFER_SIZE :
            return (char*)" CL_INVALID_BUFFER_SIZE ";
        case CL_INVALID_MIP_LEVEL :
            return (char*)" CL_INVALID_MIP_LEVEL ";
        case CL_INVALID_GLOBAL_WORK_SIZE :
            return (char*)" CL_INVALID_GLOBAL_WORK_SIZE ";
        case CL_INVALID_PROPERTY :
            return (char*)" CL_INVALID_PROPERTY ";
        default:
            return (char*)"UNKNOWN ERROR";

    }
}

int output_device_info(cl_device_id device_id)

{

    int err;                            // error code returned from OpenCL calls

    cl_device_type device_type;         // Parameter defining the type of the compute device
    cl_uint comp_units;                 // the max number of compute units on a device
    cl_char vendor_name[1024] = {0};    // string to hold vendor name for compute device
    cl_char device_name[1024] = {0};    // string to hold name of compute device

#ifdef VERBOSE
    cl_uint          max_work_itm_dims;
    size_t           max_wrkgrp_size;
    size_t          *max_loc_size;
#endif





    err = clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof(device_name), &device_name, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to access device name!\n");
        return EXIT_FAILURE;
    }
    printf(" \n Device is  %s ",device_name);


    err = clGetDeviceInfo(device_id, CL_DEVICE_TYPE, sizeof(device_type), &device_type, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to access device type information!\n");
        return EXIT_FAILURE;
    }
    if(device_type  == CL_DEVICE_TYPE_GPU)
       printf(" GPU from ");
    else if (device_type == CL_DEVICE_TYPE_CPU)
       printf("\n CPU from ");
    else 
       printf("\n non  CPU or GPU processor from ");

    err = clGetDeviceInfo(device_id, CL_DEVICE_VENDOR, sizeof(vendor_name), &vendor_name, NULL);

    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to access device vendor name!\n");
        return EXIT_FAILURE;
    }
    printf(" %s ",vendor_name);

    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &comp_units, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to access device number of compute units !\n");
        return EXIT_FAILURE;
    }
    printf(" with a max of %d compute units \n",comp_units);


#ifdef VERBOSE
//
// Optionally print information about work group sizes
//
    err = clGetDeviceInfo( device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), 
                               &max_work_itm_dims, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS)!\n",
                                                                            err_code(err));
        return EXIT_FAILURE;
    }
    
    max_loc_size = (size_t*)malloc(max_work_itm_dims * sizeof(size_t));
    if(max_loc_size == NULL){
       printf(" malloc failed\n");
       return EXIT_FAILURE;
    }
    err = clGetDeviceInfo( device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, max_work_itm_dims* sizeof(size_t), 
                               max_loc_size, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_SIZES)!\n",err_code(err));
        return EXIT_FAILURE;
    }
    err = clGetDeviceInfo( device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), 
                               &max_wrkgrp_size, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_GROUP_SIZE)!\n",err_code(err));
        return EXIT_FAILURE;
    }
   printf("work group, work item information");
   printf("\n max loc dim ");
   for(int i=0; i< max_work_itm_dims; i++)
     printf(" %d ",(int)(*(max_loc_size+i)));
   printf("\n");
   printf(" Max work group size = %d\n",(int)max_wrkgrp_size);
#endif
    return CL_SUCCESS;
}

void inicializaGPU(cl_context *context ,cl_command_queue * command_queue, cl_program *program,cl_device_id *device_id, int gpucpu ,char * programCL,int numKernels,cl_kernel kernel[],char * kernelNames, ...){



//inicializaGPU(&context ,&command_queue,&program,&device_id,0,"SumaRienReduction.cl",&kernel,"calcRectangles", "reduce"){

    cl_int err;
   // printf("Empezando IniciaLiazoGPU\n");
    FILE* programHandle;
    size_t programSize, kernelSourceSize;
    char *programBuffer, *kernelSource;

 // get size of kernel source
    programHandle = fopen(programCL,"r");
    fseek(programHandle, 0, SEEK_END);
    programSize = ftell(programHandle);
    rewind(programHandle);

 // read kernel source into buffer
    programBuffer = (char*) malloc(programSize + 1);
    programBuffer[programSize] = '\0';
    int p =  fread(programBuffer, sizeof(char), programSize, programHandle);   
    fclose(programHandle);

   //  printf("Ya he abierto y leido el fichedo %s\n",programCL);
        // Set up platform and GPU device

    cl_uint numPlatforms= 0;
    //printf("Ya hemos leido el kernel del fichero%i\n",2);
    // Find number of platforms 
    

    
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    if (err != CL_SUCCESS || numPlatforms <= 0)
    {
        printf("Error: Failed to find a platform!\n%s\n",err_code(err));
        exit(1);
    }   
    //printf("Plataformas obtenidas!%i\n",1);

    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    if (err != CL_SUCCESS || numPlatforms <= 0)
    {
        printf("Error: Failed to get the platform!\n%s\n",err_code(err));
        exit(1);
    }
   // printf("Id de Plataformas obtenidas!\n");

    // Secure a GPU
    if (gpucpu==0){

        int i;
        for (i = 0; i < numPlatforms; i++)
        {
            err = clGetDeviceIDs(Platform[i], CL_DEVICE_TYPE_CPU, 1, device_id, NULL);
            if (err == CL_SUCCESS)
            {
                break;
            }
        }
    }
    else{

        int i;
        for (i = 0; i < numPlatforms; i++)
        {
            err = clGetDeviceIDs(Platform[i], CL_DEVICE_TYPE_GPU, 1, device_id, NULL);
            if (err == CL_SUCCESS)
            {
                break;
            }
        }

    }

    //printf("CPU asegurada!\n");

    if (*device_id == NULL)
    {
        printf("Error: Failed to create a device group!\n%s\n",err_code(err)); 
        exit(1);
    }

    output_device_info(*device_id);

    // Create a compute context 
    *context = clCreateContext(0, 1, device_id, NULL, NULL, &err);  
    if (err!= CL_SUCCESS)
    {
        printf("Error: Failed to create a compute context!\n%s\n", err_code(err));
        exit(1);
    }
   // printf("Contexto listo\n"); 
    // create command queue 
    *command_queue = clCreateCommandQueue(*context,*device_id, 0, &err);
    if (err != CL_SUCCESS)
    {   
        printf("Unable to create command queue. Error Code=%s\n",err_code(err));
        exit(1);
    }
     
    // create program object from source. 
    // kernel_src contains source read from file earlier
   // printf("Comandos listos\n");
    *program = clCreateProgramWithSource(*context, 1 ,(const char**) &programBuffer, NULL, &err);
    if (err != CL_SUCCESS)
    {   
        printf("Unable to create program object. Error Code=%s\n",err_code(err));
        exit(1);
    }  
    
    const char options[] = "-cl-std=CL1.1 -cl-mad-enable -Werror";  
    err = clBuildProgram(*program, 1, device_id, options, NULL, NULL);  

    if (err != CL_SUCCESS)
    {
            printf("Build failed. Error Code=%s\n", err_code(err));

        size_t len;
        char buffer[2048];
        // get the build log
        clGetProgramBuildInfo(*program, *device_id, CL_PROGRAM_BUILD_LOG,
                                  sizeof(buffer), buffer, &len);
        printf("--- Build Log -- \n %s\n",buffer);
        exit(1);
    }
   // printf("Programa listo\n");
    

   // printf("empezamos con va_start usando  %s\n",kernelNames);
    va_list arguments;
    //char * start = kernelNames;   
    va_start ( arguments, kernelNames );
   // printf("Despues de va_start usando  %s\n",kernelNames);
    
    for (int i=0;i<numKernels;i++){
       //  printf("kernel[%d] = clCreateKernel(*program, %s, &err);\n",i,kernelNames);
         kernel[i] = clCreateKernel(*program, kernelNames, &err);
        if (err != CL_SUCCESS)
         {   
            printf("Unable to create kernel object. Error Code=%s\n",err_code(err));
            exit(1);
         }
          kernelNames = va_arg(arguments, char*);
    }
   
    va_end(arguments);


    

}



void init_seed()
{
	int seedi=1;
	FILE *fd;

	/* Generated random values between 0.00 - 1.00 */
	fd = fopen("/dev/urandom", "r");
	int p=fread( &seedi, sizeof(int), 1, fd);
	fclose (fd);
	srand( seedi );
}

void init2Drand(float **buffer, int n)
{
	int i, j;

	for (i=0; i<n; i++)
		for(j=0; j<n; j++)
			buffer[i][j] = 500.0*((float)(rand())/RAND_MAX)-500.0; /* [-500 500]*/
}

void init1Drand(float *buffer, int n)
{
	int i;

	for (i=0; i<n; i++)
		buffer[i] = 500.0*((float)(rand())/RAND_MAX)-500.0; /* [-500 500]*/
}

float *getmemory1D( int nx )
{
	int i,j;
	float *buffer;

	if( (buffer=(float *)malloc(nx*sizeof(float *)))== NULL )
	{
		fprintf( stderr, "ERROR in memory allocation\n" );
		return( NULL );
	}

	for( i=0; i<nx; i++ )
		buffer[i] = 0.0;

	return( buffer );
}


float **getmemory2D(int nx, int ny)
{
	int i,j;
	float **buffer;

	if( (buffer=(float **)malloc(nx*sizeof(float *)))== NULL )
	{
		fprintf( stderr, "ERROR in memory allocation\n" );
		return( NULL );
	}

	if( (buffer[0]=(float *)malloc(nx*ny*sizeof(float)))==NULL )
	{
		fprintf( stderr, "ERROR in memory allocation\n" );
		free( buffer );
		return( NULL );
	}

	for( i=1; i<nx; i++ )
	{
		buffer[i] = buffer[i-1] + ny;
	}

	for( i=0; i<nx; i++ )
		for( j=0; j<ny; j++ )
		{
			buffer[i][j] = 0.0;
		}

	return( buffer );
}


int check(float *GPU, float *CPU, int n)
{
	int i;

	for (i=0; i<n; i++)
		if(GPU[i]!=CPU[i])
			return(1);

	return(0);
}

void printMATRIX(float *m, int n)
{
	int i, j;

	for (i=0; i<n; i++){
		for (j=0; j<n; j++)
			printf("%3.1f ", m[i*n+j]);
		printf("\n");
	}
}


