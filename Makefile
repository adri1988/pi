ifndef CC
	CC = gcc 
endif

CCFLAGS= -O3 -std=c99

LIBS = -I /usr/local/cuda-7.5/include -lglut -lGLU -lGL -lm -lOpenCL

# Change this variable to specify the device type
# to the OpenCL device type of choice. You can also
# edit the variable in the source.
ifndef DEVICE
	DEVICE = CL_DEVICE_TYPE_CPU
endif

# Check our platform and make sure we define the APPLE variable
# and set up the right compiler flags and libraries
PLATFORM = $(shell uname -s)
ifeq ($(PLATFORM), Darwin)
	LIBS = -framework OpenCL
endif

CCFLAGS += -D DEVICE=$(DEVICE)

sumReductionGPU: main.c common.c
	$(CC) $^ $(CCFLAGS) $(LIBS) -o $@


clean:
	rm -f main






